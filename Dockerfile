FROM hypriot/rpi-node:8.1.3

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json .

RUN npm install

# Bundle app source
COPY . .

EXPOSE 8002
CMD [ "npm", "start" ]

