// define our app using express
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var packageJSON = require('./package.json');
var gpio = require('onoff').Gpio;

// Array de entradas y salidas
var e_s = [];

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8002;        // set our port

// AUX FUNCTIONS
// =============================================================================
// 
function checkES(pin){
	var requiredPIN;	
	if(e_s[pin] !== undefined){
		requiredPIN = e_s[pin];
	}else{
		e_s[pin] = new gpio(pin, 'out');
		requiredPIN = e_s[pin];
	}
	return requiredPIN;
}


// ROUTES FOR OUR API
// =============================================================================
// get an instance of the express Router
var router = express.Router();

// test route to make sure everything is working (accessed at GET http://localhost:8001/api)
router.get('/', function(req, res) {
	
	console.log('ticbox-rpi-io: packageJSON.version => ' + packageJSON.version);
	
    res.json({
		result:0,
		response:{
			version: packageJSON.version
		}
	});
});

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------

// Escritura
router.route('/di/:pin_id')
	.patch(function(req, res) {
		console.log("/di/:pin_id (req.body) => " + req.body);
		console.log("/di/:pin_id (req.params.pin_id) => " + req.params.pin_id);
		console.log("/di/:pin_id (req.body.value) => " + req.body.value);
		
		pin = checkES(req.params.pin_id);		
		pin.writeSync(req.body.value);
		
		res.json({
			result:0,
			response:{}
		});
	})
;

// Lectura
router.route('/do/:pin_id')
	// List all containers
	.get(function(req, res) {
		console.log("/do/:pin_id (req.body) => " + req.body);
		console.log("/do/:pin_id (req.params.pin_id) => " + req.params.pin_id);
		console.log("/do/:pin_id (req.body.value) => " + req.body.value);
		
		pin = checkES(req.params.pin_id);
		var value = pin.readSync();
		
		res.json({
			result:0,
			response:{
				value: value
			}
		});
	})
;


// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);