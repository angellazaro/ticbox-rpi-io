# Descarga

La descarga de la imagen se podrá realizar haciendo uso del siguente comando:

```
docker pull angellazaro/ticbox-rpi-io
```

# Arrancar contenedor

```
docker run -d -p 8002:8002 --net rpi-net --privileged --name rpi-io angellazaro/ticbox-rpi-io
```

# Arranque de watchtower

```
docker run -d --name watchtower -v /var/run/docker.sock:/var/run/docker.sock v2tec/watchtower:armhf-latest --interval 30
```

# Prerrequisitos

## Instalación de Docker

La instalación de Docker en RPI se puede realizar siguiendo estas instrucciones:

https://docs.docker.com/install/linux/docker-ce/debian

## Creación de red

```
docker network create rpi-net
```